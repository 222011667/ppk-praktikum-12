package com.example.recyclerapp;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MahasiswaRow extends RecyclerView.Adapter<MahasiswaViewHolder> {
    private ArrayList<Mahasiswa> dataList;
    public MahasiswaRow(ArrayList<Mahasiswa> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public MahasiswaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.activity_mahasiswa_row, parent,false);
        return new MahasiswaViewHolder(view);
    }
    private MahasiswaRow getCon(){
        return this;
    }
    @Override
    public void onBindViewHolder(@NonNull MahasiswaViewHolder holder, int position) {
        holder.getTxtNama().setText(dataList.get(position).getNama());
        holder.getTxtNim().setText(dataList.get(position).getNim());
        holder.getTxtNoHp().setText(dataList.get(position).getNohp());
    }

    @Override
    public int getItemCount() {
        return (this.dataList != null) ? dataList.size() : 0;
    }


}