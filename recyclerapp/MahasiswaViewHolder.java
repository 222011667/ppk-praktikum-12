package com.example.recyclerapp;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

public class MahasiswaViewHolder extends RecyclerView.ViewHolder{
    private TextView txtNama, txtNim, txtNoHp;
    public MahasiswaViewHolder(View itemView) {
        super(itemView);
        txtNama = (TextView) itemView.findViewById(R.id.txtNama);
        txtNim = (TextView) itemView.findViewById(R.id.txtNIM);
        txtNoHp = (TextView) itemView.findViewById(R.id.txtNoHP );
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama = ((TextView) view.findViewById(R.id.txtNama)).getText().toString();
                String nim = ((TextView) view.findViewById(R.id.txtNIM)).getText().toString();
                String nohp = ((TextView) view.findViewById(R.id.txtNoHP)).getText().toString();
                Toast.makeText(view.getContext(),String.format("%s%n%s%n%s",nama,nim,nohp),Toast.LENGTH_SHORT).show();
                //System.out.println(nama);
            }
        });
    }

    public TextView getTxtNama() {
        return txtNama;
    }

    public TextView getTxtNoHp() {
        return txtNoHp;
    }

    public TextView getTxtNim() {
        return txtNim;
    }
}